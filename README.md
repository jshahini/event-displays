# Create Event Displays Using VP1 (Virtual Point 1), Atlantis, and Tracer/Phoenix

Author: Lucas Flores

Updates/Current Maintainer: Jeff Shahinian

The goal of this set of scripts is to make producing an event display ready ESD file (VP1 likes these the best) for a single event as easy as knowing the run number and event number.

So let's get into it.
Log into lxplus (you don't have to do this on lxplus but having Athena installed (and cvmfs mounted) is essential. This takes a bit of work.)
```
ssh -XY username@lxplus.cern.ch # -X (or Y depending on OS) are needed for X11 forwarding for the VP1 GUI
```
Clone this repo:
```
git clone https://gitlab.cern.ch/jshahini/event-displays.git
```
Before you go to the next step, take note that a RAW data file with a size of up to a few GB will be downloaded. The example here is ~500 MB. 
This RAW file can be deleted once the ESD is created.
First, change into the directory you just cloned:
```
cd event-displays
```
The script will eventually run the reconstruction with the release that was originally used. I'm assuming this will be some flavor of R21, which is no longer supported
out of the box after the migration to `el9`. Therefore, you'll need to first setup a `centos7` container:
```
setupATLAS -c centos7
```
Now you're ready to go. You can run the driver script like this:
```
source createESDfromEventRunNumber.sh -r 359355 -e 884410772
```
The first argument is the run number and the second is the event number.
The first thing this will do is a `voms-proxy-init -voms atlas` as you will need a valid grid certificate for a few of these steps.
So you will be prompted for your password for this (if you set one).
The runtime for this script is anywhere from 5-15min, so give it some time. 
This time-suck is mostly due to the `rucio download` and reconstruction steps in the script.  

**Special Reconstruction:** The above example runs the reconstruction command that corresponds to the original ftag used. This is suitable for the majority of cases, but sometimes
you might need to run a special flavor of the reconstruction if, for example, you're doing a long-lived analysis and want to show non-standard secondary vertices from VSI.
In that case, you just need to specify the necessary rtag, and the proper reconstruction will be performed. That is done with an extra `-t` argument like this:
```
source createESDfromEventRunNumber.sh -r 359355 -e 884410772 -t r10658
```

**XML file creation:** By default, the code will produce an `xml` file that can be used for visualizing the event in Atlantis/Tracer/Phoenix. Unfortunately,
I've encountered at least one instance in which this causes the reconstruction step to fail. If you find yourself in this situation, you can disable the `xml`
file creation by using the `-x` option. You will still end up with an ESD file that can be used with VP1. Here's an example:
```
source createESDfromEventRunNumber.sh -r 359355 -e 884410772 -x
```

**Note:** This example will likely crash because the relevant `RAW` file is only available on tape. It should be obvious if this is the case, as you will see rucio complaining that
the file `only has replicas on tape` during the downloading step and the script will exit with an ERROR. See the [troubleshooting section](#troubleshooting) below for instructions on how to replicate the `RAW` file to disk.

## VP1

If everything goes smoothly the script will produce a file with the name `r359355_e884410772.f964.ESD.pool.root` (or an rtag if one was specified).
And then running: 
```
vp1 r359355_e884410772.f964.ESD.pool.root
```
will open this event in VP1. 
You can check out the VP1 documentation here: https://atlas-vp1.web.cern.ch/. Additionally, see the [VP1 Tutorials section](#vp1-tutorials) below for links to helpful material related to VP1 and making your event displays compatible with ATLAS standards.

**Tips/Example**
- At the time of writing, I have had success with VP1 on lxplus by setting up Athena like this:
```
asetup 22.0.59,Athena
```
- On my mac, I have had issues on lxplus where VP1 gives an error when saving the event displays as `.png` files. Setting this environment variable seems to fix the issue:
```
export __GLX_VENDOR_LIBRARY_NAME=mesa
```
An example VP1 config file is provided in this repository. It should serve as a reasonable starting point, drawing some of the detector geometry, muons, tracks, and jets.
It gives you the main event display, as well as a tab that corresponds to the x-y plane.
To use this config, do the following:
```
export VP1_NEXTRA_GEOSYS=5
export VP1_NEXTRA_TRKSYS=5
vp1 /path/to/myESD.root example_config.vp1
```
The first two lines allow you to have access to five different instances of the detector geometry and track collections (called `AltGeo` and `AltTrk`). The former is useful for applying different style and fiducial cuts to the detector geometry and the latter is useful, for example, when you want to draw electron tracks in a different color (the ATLAS recommendation is blue).
You will of course need to tune things to accommodate your event, e.g. putting cuts on the tracks, jets, calorimeter cells, etc.

Here are some other tips:
- If you want to draw MET, you will find several flavors of it in the Analysis tab. The phi direction of the MET is not guaranteed to match the MET you have at the analysis level, so you should make sure to pick the MET line that closely corresponds to your analysis-level MET. You can see the MET direction by going to Analysis --> Browser.
- Do you want to draw b-jets? They should also be colored blue according to the ATLAS event display standards. If you also want to draw non-btagged jets, then here's a trick: use the EMPFlow jet collection with the appropriate kinematic cuts to draw your b-jets in blue, and then use the EMTopo jet collection with the appropriate kinematic cuts to draw these in the standard yellow.


## Atlantis

An `xml` file named `JiveXML_359355_884410772.xml` will also be created.
This should be able to be used in the Atlantis event display software http://atlantis.web.cern.ch/atlantis/ (though I have not tested this myself yet).
Setup and running info here: https://twiki.cern.ch/twiki/bin/view/AtlasComputing/Atlantis

## Tracer/Phoenix

You can also use this `xml` in the super nice online tracer and phoenix event display software:

https://tracer.web.cern.ch/

https://hepsoftwarefoundation.org/phoenix/#/atlas

However, it is important to note this is almost entirely for outreach level purposes (but it is pretty cool to have your event in a little proton collision video so easily) .
Only a very crude event filtering selection is possible so it is quite difficult to only show a particular set of objects of interest. 
It is maybe also important to note that you are uploading a potentially non-public event to some web sever, so maybe setting up the software locally is the way to go. The tracer and phoenix codes are available here:

https://gitlab.cern.ch/tracer

https://github.com/HSF/phoenix

## Troubleshooting

Now there is a good chance that all this did not work on the first try.
This is most probably due to rucio complaining that it can't download the file.
This is likely because the event is on tape and a bit more work needs to be done for it to be copied to the grid where rucio can get its hands on it. 
One way to do this is to go to this site https://rucio-ui.cern.ch/ and find your data file (`data16_13TeV:data16_13TeV.00303421.physics_Main.daq.RAW._lb0474._SFO-3._0003.data` as an example).
Click through to a page that has a "File Replica States" section, and under the "Replicas" column click on the little 'download' icon :inbox_tray: .
This should start the replication of that file to some disk that exists on the grid.  

If this does not work (it didn't for me recently...), then you can submit the replication rule manually with something like this:
```
rucio add-rule --account $USER RAWfilename 1 CERN-PROD_SCRATCHDISK
```
This example will replicate the RAW file to `CERN-PROD_SCRATCHDISK` with a lifetime of two weeks. The replication will take some time. Patience is a virtue.

Other reasons things might not have worked:
* There was not a RAW file for your event. -> it is possible to use the AOD version. Right now you need to do this by hand. 
* There is a RAW file but rucio is being dumb and still won't download it -> Can download it locally first through https://rucio-ui.cern.ch/ 
* Your ESD file was created (everything went fine) but VP1 wont work -> try logging out, go back into the area with the ESD file and re-setup athena.

## Other Info

You can learn in more detail about the Event Index client that is used in the script here: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/EventIndexTutorial

## VP1 Tutorials

A short event display tutorial was produced for the 2023 SUSY Workshop in Oslo. You can find it [here](https://gitlab.cern.ch/groups/salderwe-tools/SUSY_Workshop-Oslo2023/-/wikis/ATLAS-Event-Displays/ATLAS-Event-Displays).

Additionally, here is a tutorial produced for the 2024 ATLAS Week: [VP1 tutorial](https://indico.cern.ch/event/1374726/#4-demo-vp1)

## TODO:
* Fix crash when `--postExec` option is used in the `Reco_tf` command without an argument. An example is `r438252_e1133906165`.
* Add in MC capability 
* Add in Run 3 compatibility
* Test `xml` in Atlantis
* See if Tracer can be run locally

