#!/bin/bash

# Function to display usage information
usage() {
    echo "Usage: source createESDfromEventRunNumber.sh -r <runNum> -e <evntNum> [-t <tag>] [-x]"
    return 1
}

# Function to parse options
parse_args() {
    local OPTIND
    while getopts "r:e:t:x" opt; do
        case "$opt" in
            r) runNum="$OPTARG" ;;
            e) evntNum="$OPTARG" ;;
            t) rtag="$OPTARG" ;;
            x) noXML=true ;;
            *) usage; return 1 ;;
        esac
    done

    # Check for required arguments
    if [ -z "$runNum" ] || [ -z "$evntNum" ]; then
        echo "Error: -r and -e flags are required."
        usage
        return 1
    fi
}

# Initialize variables
runNum=""
evntNum=""
rtag=""
noXML=false

# Reset OPTIND to 1 before parsing arguments
OPTIND=1
# Call the function with arguments
parse_args "$@"
if [ $? -ne 0 ]; then
    return 1
fi

# Display parsed values (for debugging)
echo "Run Number: $runNum"
echo "Event Number: $evntNum"
echo "Special r-tag?: $rtag"
echo "Skip XML creation?: $noXML"


if [ $rtag ]; then
    useRTag=true
else
    useRTag=false
fi


##################################################################
##################################################################
# After parsing the input arguments, it's time for the magic

mkdir r${runNum}_e${evntNum}
cd r${runNum}_e${evntNum}

echo "====> Setting up ATLAS software environment"
setupATLAS -q
echo "====> Setting up pyami"
lsetup pyami -q
echo "====> Setting up eiclient"
lsetup eiclient -q
echo "====> Setting up rucio"
lsetup "rucio -w" -q
voms-proxy-init -voms atlas

echo ""
echo "====> Finding the event..."
event-lookup -d AOD -D RAW "${runNum} ${evntNum}" > tmp_r${runNum}e${evntNum}.txt
guid=$(python ../createESDfromEventRunNumberHelper.py --guid tmp_r${runNum}e${evntNum}.txt)
scope=$(python ../createESDfromEventRunNumberHelper.py --scope tmp_r${runNum}e${evntNum}.txt)
echo "====> GUID:"   $guid
echo "====> Scope:"  $scope

echo ""
echo "====> Downloading the RAW file..."
rucio download --filter guid=${guid} --scope ${scope} 2>&1 | tee tmp_r${runNum}e${evntNum}_rucio.txt

# Exit if the file only exists on tape
if grep -q "only has replicas on tape" "tmp_r${runNum}e${evntNum}_rucio.txt"; then
    echo ""
    echo "====> ERROR: The file containing your event only exists on tape and cannot be downloaded. Please replicate your file to disk."
    rm tmp*_r${runNum}e${evntNum}*.txt
    return 1
fi

echo ""
echo "====> Extracting the event..."
asetup Athena,21.0.132,here
python ../createESDfromEventRunNumberHelper.py --makeEvt "${runNum}_${evntNum}" > evt.txt
theFile=$(ls ${scope})
acmd.py filter-files -s evt.txt $scope/$theFile -o "r${runNum}_e${evntNum}.RAW.data"

echo ""
echo "====> Reconstructing the event..."
if [ $useRTag = false ]; then
    ftag=$(python ../createESDfromEventRunNumberHelper.py --getFTag tmp_r${runNum}e${evntNum}.txt)
    echo "====> ftag:" $ftag
    GetTfCommand.py --AMI=f${ftag} > tmp2_r${runNum}e${evntNum}.txt
    recoCommand=$(python ../createESDfromEventRunNumberHelper.py --recoCommand tmp2_r${runNum}e${evntNum}.txt --runNum ${runNum} --evntNum ${evntNum} --ftag ${ftag} --skipXML ${noXML})
    export evalRecoCommand=${recoCommand}
    eval ${evalRecoCommand}
else
    echo "====> rtag:" $rtag
    GetTfCommand.py --AMI=${rtag} > tmp2_r${runNum}e${evntNum}.txt
    recoCommand=$(python ../createESDfromEventRunNumberHelper.py --recoCommand tmp2_r${runNum}e${evntNum}.txt --runNum ${runNum} --evntNum ${evntNum} --rtag ${rtag} --skipXML ${noXML})
    export evalRecoCommand=${recoCommand}
    eval ${evalRecoCommand}
fi

rm tmp*_r${runNum}e${evntNum}*.txt
