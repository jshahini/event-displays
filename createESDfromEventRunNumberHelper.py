import os, sys, time, shutil
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--guid",        dest='guid',        default="", help="")
parser.add_argument("--scope",       dest='scope',       default="", help="")
parser.add_argument("--makeEvt",     dest='evt',         default="", help="")
parser.add_argument("--getFTag",     dest='getFTag',     default="", help="")
parser.add_argument("--getRTag",     dest='getRTag',     default="", help="")
parser.add_argument("--recoCommand", dest='recoCommand', default="", help="")
parser.add_argument("--runNum",      dest='runNum',      default="", help="")
parser.add_argument("--evntNum",     dest='evntNum',     default="", help="")
parser.add_argument("--ftag",        dest='ftag',        default="", help="")
parser.add_argument("--rtag",        dest='rtag',        default="", help="")
parser.add_argument("--skipXML",     dest='skipXML',     default="", help="")

args = parser.parse_args()

if args.guid != "":
  hashfile = open(args.guid, 'r')
  #for isRAW in hashfile: 
  isRAW=hashfile.readline()
  guid = isRAW.split(' ')[2]
  scope = isRAW.split(' ')[4]
  print(guid)

if args.scope != "":
  hashfile = open(args.scope, 'r')
  #for isRAW in hashfile:
  isRAW=hashfile.readline()
  scope = isRAW.split(' ')[4]
  print(scope)

if args.evt != "":
  runNum   = args.evt.split("_")[0]
  eventNum = args.evt.split("_")[1]
  print("%s %s" % (runNum.zfill(8), eventNum))

if args.getFTag != "":
  hashfile = open(args.getFTag, 'r')
  for isRAW in hashfile:
    tags =  isRAW.split(' ')[8]
    if "f" not in tags: continue
    ftagindex = tags.find("f")
    ftag      = tags[ftagindex+1:].split("_")[0]
    print(ftag)
    break

# Not totally validated/supperted at the moment, but
# this can be used to run the reconstruction based on
# the original rtag associated with your event
# instead of the ftag, which is the current
# default strategy.
# Instead, one can specify a specific rtag if needed
# as mentioned in the README
if args.getRTag != "":
  hashfile = open(args.getRTag, 'r')
  for isRAW in hashfile:
    tags =  isRAW.split(' ')[8]
    if "r" not in tags: continue
    if tags.count("r") > 1: continue
    rtagindex = tags.find("r")
    rtag      = tags[rtagindex+1:].split("_")[0]
    print(rtag)
    break

if args.recoCommand != "" :
  recoCommand = open(args.recoCommand, 'r')
  recoCommand = recoCommand.readlines()
  setupReleaseIndex = [i for i, s in enumerate(recoCommand) if 'asetup' in s]
  setupRelease = recoCommand[setupReleaseIndex[0]] 
  recoCommandIndex = [i for i, s in enumerate(recoCommand) if 'Reco_tf.py' in s]
  recoCommand = recoCommand[recoCommandIndex[0]]
  recoCommand = recoCommand.split(" --preExec ")
  if args.skipXML == "true":
    recoCommand[0] = recoCommand[0]+" --preExec  "+recoCommand[1]
  else:
    recoCommand[0] = recoCommand[0]+" --preExec 'all:jp.Rec.doJiveXML.set_Value_and_Lock(True)' "+recoCommand[1]
  " ".join(recoCommand)
  recoCommand = recoCommand[0]
  runNum = args.runNum
  evntNum = args.evntNum
  if len(args.ftag) > 0:
    recoCommand += ' --inputBSFile "r%s_e%s.RAW.data" --outputESDFile "r%s_e%s.f%s.ESD.pool.root"'%(runNum, evntNum, runNum, evntNum, args.ftag)
  else:
    recoCommand += ' --inputBSFile "r%s_e%s.RAW.data" --outputESDFile "r%s_e%s.%s.ESD.pool.root"'%(runNum, evntNum, runNum, evntNum, args.rtag)
  recoCommand = setupRelease.strip("\n") +"; "+ recoCommand 
  print(recoCommand)
